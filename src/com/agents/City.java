package com.agents;

public enum City {
    TORONTO(1,"Toronto"),
    NEW_YORK(2,"New York"),
    CALGARY(3,"Calgary"),
    CHICAGO(4,"Chicago"),
    URBANA(5,"Urbana"),
    HOUSTON(6,"Houston"),
    LOS_ANGELES(7,"Los Angeles"),
    DENVER(8,"Denver");

    private String value;
    private int number;
    City(int number, String value) {
        this.number = number;this.value = value;
    }

    public int getNumber() {
        return number;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
