package com.agents;

import com.graphs.Edge;
import com.graphs.Graph;
import com.graphs.Vertex;

import java.util.*;

public class Main {
    private Graph<City> locationsGraph;

    private Main() {
        locationsGraph = new Graph<>();
        for (City city :
                City.values()) {
            locationsGraph.addVertex(city);
        }
        locationsGraph.addEdge(City.NEW_YORK, City.CHICAGO,1000);
        locationsGraph.addEdge(City.CHICAGO, City.DENVER,1000);
        locationsGraph.addEdge(City.NEW_YORK, City.TORONTO,800);
        locationsGraph.addEdge(City.NEW_YORK, City.DENVER,1900);
        locationsGraph.addEdge(City.DENVER, City.LOS_ANGELES,1000);
        locationsGraph.addEdge(City.DENVER, City.HOUSTON,1500);
        locationsGraph.addEdge(City.DENVER, City.URBANA,1000);
        locationsGraph.addEdge(City.TORONTO,City.LOS_ANGELES,1800);
        locationsGraph.addEdge(City.TORONTO,City.CHICAGO,500);
        locationsGraph.addEdge(City.TORONTO, City.CALGARY,1500);
        locationsGraph.addEdge(City.TORONTO,City.DENVER,1900);
        locationsGraph.addEdge(City.HOUSTON, City.LOS_ANGELES,1500);
    }

    private void search(City start, City end) {
        Map<String,Integer> solutions = new HashMap<>();
        this.locationsGraph.reset();
        // Depth first
        System.out.println("Depth First");
        List<Edge<City>> citiesDF = this.locationsGraph.depthFirstSearch(start,end);
        System.out.print("From "+start.getValue());
        printPath(citiesDF);
        solutions.put("Depth First",printDistance(citiesDF));
        this.locationsGraph.reset();

        System.out.println();
        // Breadth first
        System.out.println("Breadth First");
        List<Vertex<City>> citiesBFV = this.locationsGraph.breadthFirstSearch(start,end);
        List<Edge<City>> citiesBF = vertexToEdgeList(citiesBFV,locationsGraph.getVertex(start));
        System.out.print("From "+start.getValue());
        printPath(citiesBF);
        solutions.put("Breadth First",printDistance(citiesBF));
        this.locationsGraph.reset();


        System.out.println();
        // Colina
        System.out.println("Colina");

        List<Vertex<City>> citiesColinaV = this.locationsGraph.colinaSearch(start,end);
//        System.out.println(citiesColinaV);
        List<Edge<City>> citiesColina = vertexToEdgeList(citiesColinaV,locationsGraph.getVertex(start));
        System.out.print("From "+start.getValue());
        printPath(citiesColina);
        solutions.put("Colina",printDistance(citiesColina));
        this.locationsGraph.reset();

        System.out.println();
        // Min value
        System.out.println("Min value");

        List<Vertex<City>> citiesMinValueV = new ArrayList<>();
        citiesMinValueV.add(locationsGraph.getVertex(start));
        citiesMinValueV.addAll(this.locationsGraph.minValueSearch(start,end));
        List<Edge<City>> citiesMinValue = vertexToEdgeList(citiesMinValueV,locationsGraph.getVertex(start));
        System.out.print("From "+start.getValue());
        printPath(citiesMinValue);
        solutions.put("Min value",printDistance(citiesMinValue));

        calculateBest(solutions);

    }

    private void calculateBest(Map<String, Integer> solutions) {
        Integer best = null;
        String bestKey = "";
        for (Map.Entry<String, Integer> entry : solutions.entrySet())
        {
            if(best==null||entry.getValue()<best){
                bestKey = entry.getKey();
                best = entry.getValue();
            }
        }
        System.out.println( );
        if(best>0)
         System.out.println("The best solution is "+bestKey+" with weigth of "+best);
        else
            System.out.println("No solution found");
        System.out.println( );
    }

    private void printPath(List<Edge<City>> cityList) {
        if(cityList.isEmpty()){
            System.out.println(" no suitable solution found");
        }else{
            cityList.forEach(edge -> {
                if(edge!=null) {
                    System.out.print(" -> "+edge.getEndVertex().getValue().getValue());
                }
            });
            System.out.println();
        }
    }


    private int printDistance(List<Edge<City>> cityList) {
        int distance = calculateDistance(cityList);
        if(distance>0)System.out.println(distance);
        return distance;
    }

    private List<Edge<City>> vertexToEdgeList(List<Vertex<City>> cityList, Vertex<City> startVertex) {
        //System.out.println("Vertex to convert "+ cityList);
        List<Edge<City>> res = new ArrayList<>();
        boolean end= false;
        for(int i = cityList.size()-1; i>=0&&!end; i--){
            boolean found = false;
            Vertex<City> vertexEnd = cityList.get(i);
            for(int j = 0; j<cityList.size()&&!found;j++){
                if(i!=j){
                    Vertex<City> vertexCompare = cityList.get(j);
                    if(vertexCompare!=null){
                        for(Edge<City> edge: vertexCompare.getEdgeList()){
                            if(edge.getEndVertex().equals(vertexEnd)){
                                //System.out.println("Adding edge "+edge);
                                res.add(edge);
                                if(edge.getStartVertex().equals(startVertex)){
                                    //System.out.println("I will end");
                                    end = true;
                                }
                                found=true;
                                break;
                            }
                        }
                    }
                }
            }
        }
        if(!end)return new ArrayList<>();
        Collections.reverse(res);
        return res;
    }
    private int calculateDistance(List<Edge<City>> edgeList){
        int total = 0;
        try{
            for (Edge<City> edge : edgeList) {
                if (edge != null) {
//                System.out.println(edge);
                    total += edge.getEdgeVal();
                }
            }
        }catch (Exception e){
            // pass
        }
        return total;
    }

    public static City findCityById(int id){
        return Arrays.stream(City.values())
                .filter(city->city.getNumber()==id)
                .findFirst()
                .orElse(null);
    }
    public static void main(String[] args) {
        Main main =  new Main();
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("Enter command \"quit\" or \"search\"");
            String command = scanner.next();
            if(command.equalsIgnoreCase("quit")){
                break;
            }else {
                if(command.equalsIgnoreCase("search")){
                    Arrays.stream(City.values()).forEach(city->System.out.println(" "+city.getNumber()+". "+city));
                    City start = null;
                    while (start==null){
                        System.out.println("Select start city:");
                        start = findCityById(scanner.nextInt());
                    }
                    City end = null;
                    while (end==null){
                        System.out.println("Select end city:");
                        end = findCityById(scanner.nextInt());
                    }
                    main.search(start,end);
                }
            }
        }
    }
}
