package com.graphs;

import java.util.*;

public class Vertex<T> {
    private T value;
    private List<Edge<T>> edgeList;
    private Edge<T> parentEdge;

    public T getValue() {
        return value;
    }


    Vertex(T value) {
        this.value = value;
        this.edgeList = new ArrayList<>();
        this.parentEdge = null;
    }
    Vertex(T value,Edge<T> parentEdge) {
        this.value = value;
        this.edgeList = new ArrayList<>();
        this.parentEdge = parentEdge;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o) ||this.value.equals(o);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    public void addEdge(Integer edgeVal, Vertex<T> vertexB) {
        Edge<T> edge = new Edge<>(edgeVal,this,vertexB);
        edgeList.add(edge);
        vertexB.parentEdge=edge;
    }

    public void reset() {
        this.edgeList.forEach(Edge::reset);
    }

    public List<Edge<T>> depthFirstSearch(T end) {
        List<Edge<T>> path = new ArrayList<>();
        if(this.equals(end)){
            path.add(null);
        }
        for (Edge<T> edge : edgeList) {
            if(!edge.isVisited()){
                edge.visit();
                Vertex<T> vertex = edge.getEndVertex();
                List<Edge<T>> vertexPath = vertex.depthFirstSearch(end);
                if (!vertexPath.isEmpty()){
                    path.add(edge);
                    path.addAll(vertexPath);
                    break;
                }
            }
        }
        return path;
    }
    public List<Edge<T>> getEdgeList() {
        return edgeList;
    }
    public boolean hasChildVertex(Vertex<T> childVertex){
        for (Edge<T> edge:
             edgeList) {
            if(edge.getEndVertex().equals(childVertex))return true;
        }return false;
    }
    @Override
    public String toString() {
        return  value.toString();
    }
}
