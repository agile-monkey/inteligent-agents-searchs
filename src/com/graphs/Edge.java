package com.graphs;

import java.util.Objects;

public class Edge<T> {

    private Integer edgeVal;
    private Vertex<T> endVertex;
    private Vertex<T> startVertex;
    private boolean visited;
    private boolean path;

    Edge(Integer edgeVal, Vertex<T> startVertex, Vertex<T> endVertex) {
        this.edgeVal = edgeVal;
        this.startVertex = startVertex;
        this.endVertex = endVertex;
        visited = false;
    }

    public Vertex<T> getStartVertex() {
        return startVertex;
    }

    public void setStartVertex(Vertex<T> startVertex) {
        this.startVertex = startVertex;
    }

    public Integer getEdgeVal() {
        return edgeVal;
    }

    public Vertex<T> getEndVertex() {
        return endVertex;
    }

    public void reset() {
        this.visited = false;
        endVertex.reset();
    }

    public boolean isVisited() {
        return visited;
    }

    public void visit() {
        this.visited = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge<?> edge = (Edge<?>) o;
        return visited == edge.visited &&
                path == edge.path &&
                Objects.equals(edgeVal, edge.edgeVal) &&
                Objects.equals(endVertex, edge.endVertex) &&
                Objects.equals(startVertex, edge.startVertex);
    }

    @Override
    public int hashCode() {

        return Objects.hash(edgeVal, endVertex, startVertex, visited);
    }

    @Override
    public String toString() {
        return "Edge{" +
                "startVertex=" + startVertex +
                ", endVertex=" + endVertex +
                ", edgeVal=" + edgeVal +
                '}';
    }

    public void makePath(boolean isPath){
        this.path = isPath;
    }
    public boolean isPath() {
        return path;
    }

    public void setPath(boolean path) {
        this.path = path;
    }
}
