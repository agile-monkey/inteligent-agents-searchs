package com.graphs;

import java.util.*;

public class Graph<T> {
    private List<Vertex<T>> shortestPath = new ArrayList<>();

    private List<Vertex<T>> vertexList;

    public Graph() {
        vertexList = new ArrayList<>();
    }

    public void addVertex(T vertexVal){
        Vertex<T> vertex = new Vertex<>(vertexVal);
        if(!vertexList.contains(vertex))this.vertexList.add(vertex);
    }

    public void addEdge(T vertexAVal, T vertexBVal, Integer edgeVal) {
            Vertex<T> vertexA = this.vertexList.stream()
                    .filter(vertex->vertex.equals(vertexAVal))
                    .findFirst()
                    .orElse(new Vertex<>(vertexAVal));
            Vertex<T> vertexB = this.vertexList.stream()
                    .filter(vertex->vertex.equals(vertexBVal))
                    .findFirst()
                    .orElse(new Vertex<>(vertexBVal));
            vertexA.addEdge(edgeVal, vertexB);
    }

    public void reset() {
        this.vertexList.forEach(Vertex::reset);
    }

    public Vertex<T> getVertex(T vertexValue) {
        return this.vertexList.stream()
                .filter(vertex->vertex.equals(vertexValue))
                .findFirst()
                .orElse(null);
    }

    public List<Edge<T>> depthFirstSearch(T start, T end) {
        Vertex<T> startVertex = getVertex(start);
        return startVertex.depthFirstSearch(end);
    }

    public List<Vertex<T>> breadthFirstSearch(T start, T end) {
        Vertex<T> startVertex = getVertex(start);
        Queue<Vertex<T>> queue = new LinkedList<>();
        Queue<Vertex<T>> queueRes = new LinkedList<>();
        queue.add(startVertex);
        boolean found=false;
        while(!queue.isEmpty()&&!found){
            Vertex<T> vertex = queue.remove();
            queueRes.add(vertex);
            Edge<T> childEdge;
            while((childEdge=getUnvisitedEdge(vertex))!=null&&!found) {
                childEdge.visit();
                //System.out.println(childEdge+" ");
                if(childEdge.getEndVertex().equals(end)){
                    queueRes.add(childEdge.getEndVertex());
                    found = true;
                }
                queue.add(childEdge.getEndVertex());
                Collections.reverse((List<?>) queueRes);
                Vertex<T> vertexOut = queueRes.poll();
                Collections.reverse((List<?>) queueRes);
                queueRes.add(vertexOut);
                //System.out.println(queueRes);
                //System.out.println();
            }
        }
        //System.out.println(queueRes);
        return new ArrayList<>(queueRes);
    }

    private Edge<T> getUnvisitedEdge(Vertex<T> vertex){
        for (Edge<T> edge : vertex.getEdgeList()) {
            if (!edge.isVisited()){
                return edge;
            }
        }
        return null;
    }
    public List<Vertex<T>> colinaSearch(T start, T end) {
        Vertex<T> startVertex = getVertex(start);
        Queue<Vertex<T>> queue = new LinkedList<>();
        Queue<Vertex<T>> queueRes = new LinkedList<>();
        queue.add(startVertex);
        boolean found=false;
        while(!queue.isEmpty()&&!found){
            Vertex<T> vertex = queue.remove();
            queueRes.add(vertex);
            Edge<T> childEdge;
            while((childEdge=getUnvisitedEdgeMaxValue(vertex))!=null&&!found) {
                childEdge.visit();
//                System.out.println(childEdge+" ");
                if(childEdge.getEndVertex().equals(end)){
                    queueRes.add(childEdge.getEndVertex());
                    found = true;
                }
                queue.add(childEdge.getEndVertex());
//                System.out.println(queueRes);
//                System.out.println();
            }
        }
        return new ArrayList<>(queueRes);
    }
    public List<Vertex<T>> minValueSearch(T start, T end) {
        Vertex<T> startVertex = getVertex(start);
        Queue<Vertex<T>> queue = new LinkedList<>();
        Queue<Vertex<T>> queueRes = new LinkedList<>();
        queue.add(startVertex);
        boolean found=false;
        while(!queue.isEmpty()&&!found){
            Vertex<T> vertex = queue.remove();
            queueRes.add(vertex);
            Edge<T> childEdge;
            while((childEdge=getUnvisitedEdgeMinValue(vertex))!=null&&!found) {
                childEdge.visit();
//                System.out.println(childEdge+" ");
                if(childEdge.getEndVertex().equals(end)){
                    queueRes.add(childEdge.getEndVertex());
                    found = true;
                }
                queue.add(childEdge.getEndVertex());
//                System.out.println(queueRes);
//                System.out.println();
            }
        }
        return new ArrayList<>(queueRes);
    }
    private Edge<T> getUnvisitedEdgeMaxValue(Vertex<T> vertex) {
        Edge<T> maxEdge = null;
        for (Edge<T> edge : vertex.getEdgeList()) {
            if (!edge.isVisited()&&(maxEdge==null||maxEdge.getEdgeVal()<edge.getEdgeVal())){
                maxEdge =  edge;
            }
        }
        return maxEdge;
    }
    private Edge<T> getUnvisitedEdgeMinValue(Vertex<T> vertex) {
        Edge<T> maxEdge = null;
        for (Edge<T> edge : vertex.getEdgeList()) {
            if (!edge.isVisited()&&(maxEdge==null||maxEdge.getEdgeVal()>edge.getEdgeVal())){
                maxEdge =  edge;
            }
        }
        return maxEdge;
    }
}
