package com.graphs.exeptions;

import com.graphs.Vertex;

public class AlreadyExistingVertexException extends Exception {
    public AlreadyExistingVertexException(Vertex<?> vertex) {
        super("The vertex "+vertex.toString()+" already exist");
    }
}
