package com.graphs.exeptions;

import com.graphs.Vertex;

public class NonExistingVertexException extends Exception{
    public NonExistingVertexException(Vertex<?> vertex) {
        super("The vertex "+vertex.toString()+" doesn't exist");
    }
}
